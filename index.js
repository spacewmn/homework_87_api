const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const app = express();
const port = 8000;
const artists = require("./app/artists");
const albums = require("./app/albums");
const tracks = require("./app/tracks")
const users = require("./app/users");
const trackHistory = require("./app/trackHistories");
const config = require('./config');

app.use(cors());
app.use(express.json());
app.use(express.static("public"));

const run = async () => {
    await mongoose.connect(config.db.url + '/' + config.db.name, {useNewUrlParser: true, autoIndex: true});

    app.use("/artists", artists());
    app.use("/albums", albums());
    app.use("/tracks", tracks());
    app.use("/users", users);
    app.use("/track_history", trackHistory)

    console.log("Connected to mongo DB");

    app.listen(port, () => {
        console.log(`Server started at http://localhost:${port}`);
    });
};

run().catch(console.log);