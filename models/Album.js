const mongoose = require('mongoose');
const idValidator = require('mongoose-id-validator');

const Schema = mongoose.Schema;

const AlbumSchema = new Schema({
   title: {
       type: String,
       required: true
   },
    artist: {
      type: Schema.Types.ObjectId,
      ref: "Artist",
      require: true
    },
   year: {
       type: Number,
       required: true
   },
   image: String,
    publish: {
        type: Boolean,
        default: false
    }
});
AlbumSchema.plugin(idValidator, {
    message: 'Such {PATH} doesn`t exist'
});


const Album = mongoose.model("Album", AlbumSchema);
module.exports = Album;