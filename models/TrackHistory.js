const mongoose = require('mongoose');
const idValidator = require('mongoose-id-validator');

const Schema = mongoose.Schema;

const TrackHistorySchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: "User",
    },
    track: {
        type: Schema.Types.ObjectId,
        ref: "Track",
        require: true
    },
    datetime: {
        type: Date,
        default: Date.now
    }
});

TrackHistorySchema.plugin(idValidator, {
    message: 'Such {PATH} doesn`t exist'
});


const TrackHistory = mongoose.model('TrackHistory', TrackHistorySchema);
module.exports = TrackHistory;