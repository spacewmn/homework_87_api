const mongoose = require('mongoose');
const config = require('./config');
const Album = require('./models/Album');
const Artist = require('./models/Artist');
const Track = require('./models/Track');
const User = require('./models/User');
const TrackHistory = require('./models/TrackHistory');
const {nanoid} = require('nanoid');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
    try {
        await db.dropCollection('albums');
        await db.dropCollection('artists');
        await db.dropCollection('tracks');
        await db.dropCollection('track_history');
        await db.dropCollection('users');
    } catch (e) {
        console.log('Collections were not presented, skipping drop...');
    }

    const [DojaArtist, JojiArstist] = await Artist.create({
        title: 'Doja Cat',
        image: 'doja.jpeg',
        publish: true,
        info: 'Better known as Doja Cat (Dodge Cat)  - American singer, rapper, author, producer. She became famous thanks to the viral hit of 2018 "Mooo!", Which had great success on the YouTube channel and was included in the lists of the best videos of the year'
    }, {
        title: 'Joji',
        image: 'joji.jpg',
        publish: false,
        info: 'Joji ( Joji ) and past aliases Filthy Frank and of Pink Guy - Japanese and Australian videobloger , singer , writer , rapper , composer and former internet celebrity and comedian .'
    });

    const [PinkAlbum, NectarAlbum] = await Album.create({
        title: 'Hot Pink',
        artist: DojaArtist._id,
        year: 2019,
        publish: true,
        image: 'Hot_Pink.jpg'
    }, {
        title: 'Nectar',
        artist: JojiArstist._id,
        year: 2020,
        publish: false,
        image: 'nectar.png'
    });

    const [RulesTrack, LoveTrack] = await Track.create({
        title: 'Rules',
        album: PinkAlbum._id,
        publish: true,
        duration: 4
    }, {
        title: 'Gimme Love',
        album: NectarAlbum._id,
        publish: false,
        duration: 3
    });

    const [Admin, ArchiUser] = await User.create({
        username: 'admin',
        email: 'admin@asd.asd',
        password: 'Admin123!',
        token: nanoid(),
        role: 'admin'
    }, {
        username: 'Archi',
        email: 'archi@asd.asd',
        password: 'Archi123!',
        token: nanoid(),
        role: 'user'
    });

    await TrackHistory.create({
        user: Admin._id,
        track: RulesTrack._id,
    }, {
        user: ArchiUser._id,
        track: LoveTrack._id,
    });
    db.close();
});