const router = require("express").Router();
const Tracks = require("../models/Track");
const auth = require("../middlware/auth");


const createRouter = () => {
    router.get("/", async (req, res) => {
        let query;
        if (req.query.album) {
            query = {album: req.query.album}
        }
        console.log(query);
        try {
            const tracks = await Tracks.find(query).populate('album');
            res.send(tracks);
        } catch (e) {
            res.sendStatus(500);
        }
    });
    router.post("/", async (req, res) => {
        const trackData = req.body;
        const track = new Tracks(trackData);
        try {
            await track.save();
            res.send(track);
        } catch(e) {
            console.log(e);
            res.status(400).send(e);
        }
    });
    router.put("/:id",auth, async (req, res) => {
        const trackData = req.body;

        try {
            if (req.user.role === 'admin') {
                const track = await Tracks.findByIdAndUpdate(req.params.id, trackData)
                await track.save();
                res.send(track);
            }
        } catch(e) {
            res.sendStatus(400);
        }
    });
    router.delete("/:id", auth, async (req, res) => {
        try {
            if (req.user.role === 'admin') {
                const track = await Tracks.findByIdAndUpdate(req.params.id);
                await track.delete();
                res.send('Deleted');
            }
        } catch(e) {
            res.sendStatus(400);
        }
    });
    return router;
};

module.exports = createRouter;