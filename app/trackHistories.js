const router = require("express").Router();
const TrackHistories = require("../models/TrackHistory");
const User = require('../models/User');
const auth = require("../middlware/auth");


    router.get("/", auth, async (req, res) => {
        try {
            const trackHistories = await TrackHistories.find({"user": req.user._id})
                .populate({
                    path: 'track',
                    populate: {
                        path: "album",
                        populate: {
                            path: 'artist'
                        }
                    }
                })
            res.send(trackHistories);
        } catch (e) {
            res.sendStatus(500);
        }
    });

router.post("/", auth, async (req, res) => {
        const userId = req.user._id;
    // const userInfo = {...req.body}
    const userInfo = {...req.body, user: userId}
        let trackHistory = new TrackHistories(userInfo);
        try {
            await trackHistory.save();
            res.send(trackHistory);
        } catch(e) {
            console.log(e);
            res.status(400).send(e);
        }
    });

module.exports = router;