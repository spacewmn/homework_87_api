const router = require("express").Router();
const multer = require("multer");
const path = require("path");
const {nanoid} = require("nanoid");
const config = require("../config");
const Artists = require("../models/Artist");
const auth = require("../middlware/auth");
const permit = require("../middlware/permit");


const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = () => {
    router.get("/", auth, async (req, res) => {
        // const token = req.get("Authorization");
        // console.log(req.user);
        let artists;

        try {
            // let artists;
            // if(!!token){
            //     artists = await Artists.find({publish: true});
            // }
            if (req.user.role === 'admin') {
                artists = await Artists.find();
            } else {
                artists = await Artists.find({publish: true});
            }
            // console.log(req.user, 'user');

            res.send(artists);
        } catch (e) {
            res.sendStatus(500);
        }
    });
    router.post("/", upload.single("image"), async (req, res) => {
        const artistData = req.body;
        if (req.file) {
            artistData.image = req.file.filename;
        }
        const artist = new Artists(artistData);
        try {
            await artist.save();
            res.send(artist);
        } catch(e) {
            res.sendStatus(400);
        }
    });
    router.put("/:id",auth, async (req, res) => {
        const artistData = req.body;

        try {
            if (req.user.role === 'admin') {
                const artist = await Artists.findByIdAndUpdate(req.params.id, artistData)
                await artist.save();
                res.send(artist);
            }
        } catch(e) {
            res.sendStatus(400);
        }
    });
    router.delete("/:id", auth, async (req, res) => {
        // const artistData = req.body;

        try {
            if (req.user.role === 'admin') {
                const artist = await Artists.findByIdAndUpdate(req.params.id);
                await artist.delete();
                res.send('Deleted');
            }
        } catch(e) {
            res.sendStatus(400);
        }
    });


    return router;
};

module.exports = createRouter;