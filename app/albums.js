const router = require("express").Router();
const multer = require("multer");
const path = require("path");
const {nanoid} = require("nanoid");
const config = require("../config");
const Albums = require("../models/Album");
const auth = require("../middlware/auth");


const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = () => {
    router.get("/", auth, async (req, res) => {
        let query;
        if (req.query.artist) {
            query = {artist: req.query.artist}
        }
        try {
            let albums;
            if (req.user.role === 'admin') {
                albums = await Albums.find(query).populate('artist');
            } else {
                albums = await Albums.find({publish: true});
            }

            res.send(albums);
        } catch (e) {
            res.sendStatus(500);
        }
    });
    router.get("/:id", async (req, res) => {
        let query = await Albums.findById(req.params.id);
        if (req.query.artist) {
            query = {artist: req.query.artist}
        }
        if (query) {
            const albums = await Albums.find(query).populate('artist');
            res.send(albums);
        } else {
            res.sendStatus(404);
        }
    });
    router.post("/", upload.single("image"), async (req, res) => {
        const albumData = req.body;
        console.log(albumData);
        if (req.file) {
            albumData.image = req.file.filename;
        }

        const album = new Albums(albumData);
        try {
            await album.save();
            res.send(album);
        } catch(e) {
            res.status(400).send(e);
        }
    });
    router.put("/:id",auth, async (req, res) => {
        const albumData = req.body;

        try {
            if (req.user.role === 'admin') {
                const album = await Albums.findByIdAndUpdate(req.params.id, albumData)
                await album.save();
                res.send(album);
            }
        } catch(e) {
            res.sendStatus(400);
        }
    });
    router.delete("/:id", auth, async (req, res) => {
        try {
            if (req.user.role === 'admin') {
                const album = await Albums.findByIdAndUpdate(req.params.id);
                await album.delete();
                res.send('Deleted');
            }
        } catch(e) {
            res.sendStatus(400);
        }
    });

    return router;
};

module.exports = createRouter;